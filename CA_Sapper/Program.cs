﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_Sapper
{
    class Program
    {       
        static Random rnd = new Random();

        enum Cell
        {
            Empty,
            OneMine,
            TwoMines,
            ThreeMines,
            FourMines,
            FiveMines,
            SixMines,
            SevenMines,
            EightMines,
            Flag,
            Bomb,
            OutSpace,
            CloseCell
        }

        enum Level
        {
            Beginner,
            Medium,
            Professional
        }

        static Level ChooseLevel()
        {
            int choosenLevel = -1;
            Console.WriteLine("Выберите уровень сложности (0 - новичок, 1 - средний уровень, 2 - профессионал)");
            do
            {
                choosenLevel = int.Parse(Console.ReadLine());

            } while (choosenLevel != 0 && choosenLevel != 1 && choosenLevel != 2);

            return (Level)choosenLevel;
        }

        static void LevelParams(Level userLevel, out int fieldSizeI, out int fieldSizeJ, out int bombsCount)
        {
            fieldSizeI = 0;
            fieldSizeJ = 0;
            bombsCount = 0;

            switch (userLevel)
            {
                case Level.Beginner:
                    fieldSizeI = 9;
                    fieldSizeJ = 9;
                    bombsCount = 10;
                    break;
                case Level.Medium:
                    fieldSizeI = 16;
                    fieldSizeJ = 16;
                    bombsCount = 40;
                    break;
                case Level.Professional:
                    fieldSizeI = 16;
                    fieldSizeJ = 30;
                    bombsCount = 99;
                    break;
            }
        }

        static void ClearField(Cell[,] field)
        {
            for (int i = 0; i < field.GetLength(0); i++)
            {
                for (int j = 0; j < field.GetLength(1); j++)
                {
                    field[i, j] = Cell.CloseCell;
                }
            }
        }

        static void RandomPlaceMines(Cell[,] field, int bombsCount)
        {
            int iCell, jCell;
            for (int k = 0; k < bombsCount; k++)
            {
                do
                {
                    iCell = rnd.Next(0, field.GetLength(0));
                    jCell = rnd.Next(0, field.GetLength(1));
                } while (field[iCell, jCell] != Cell.CloseCell);

                field[iCell, jCell] = Cell.Bomb;
            }
        }

        static void PlaceMinesCount(Cell[,] field, int iCell, int jCell, int bombsCount)
        {
            if (field[iCell, jCell] != Cell.Bomb)
            {
                switch (bombsCount)
                {
                    case 0:
                        field[iCell, jCell] = Cell.Empty;
                        break;
                    case 1:
                        field[iCell, jCell] = Cell.OneMine;
                        break;
                    case 2:
                        field[iCell, jCell] = Cell.TwoMines;
                        break;
                    case 3:
                        field[iCell, jCell] = Cell.ThreeMines; ;
                        break;
                    case 4:
                        field[iCell, jCell] = Cell.FourMines;
                        break;
                    case 5:
                        field[iCell, jCell] = Cell.FiveMines;
                        break;
                    case 6:
                        field[iCell, jCell] = Cell.SixMines;
                        break;
                    case 7:
                        field[iCell, jCell] = Cell.SevenMines;
                        break;
                    case 8:
                        field[iCell, jCell] = Cell.EightMines;
                        break;
                }
            }
        }

        static void FillHiddenField(Cell[,] field, int bombsCount)
        {
            int nearbyBombs;

            Cell[,] tmpField = new Cell[field.GetLength(0) + 2, field.GetLength(1) + 2];

            for (int i = 0; i < tmpField.GetLength(0); i++)
             {
                 for (int j = 0; j < tmpField.GetLength(1); j++)
                 {
                     if (i == 0 || j == 0 || i == tmpField.GetLength(0) - 1 || j == tmpField.GetLength(1) - 1)
                     {
                        tmpField[i, j] = Cell.OutSpace;
                     }
                 }
             }

            for (int i = 1; i < tmpField.GetLength(0) - 1; i++)
            {
                for (int j = 1; j < tmpField.GetLength(1) - 1; j++)
                {
                    tmpField[i, j] = field[i - 1, j - 1];
                }
            }

            for (int i = 1; i < tmpField.GetLength(0) - 1; i++)
            {
                for (int j = 1; j < tmpField.GetLength(1) - 1; j++)
                {
                    nearbyBombs = 0;
                    if (tmpField[i - 1, j - 1] == Cell.Bomb)
                    {
                        nearbyBombs++;
                    }
                    if (tmpField[i - 1, j] == Cell.Bomb)
                    {
                        nearbyBombs++;
                    }
                    if (tmpField[i - 1, j + 1] == Cell.Bomb)
                    {
                        nearbyBombs++;
                    }
                    if (tmpField[i, j - 1] == Cell.Bomb)
                    {
                        nearbyBombs++;
                    }
                    if (tmpField[i, j + 1] == Cell.Bomb)
                    {
                        nearbyBombs++;
                    }
                    if (tmpField[i + 1, j - 1] == Cell.Bomb)
                    {
                        nearbyBombs++;
                    }
                    if (tmpField[i + 1, j] == Cell.Bomb)
                    {
                        nearbyBombs++;
                    }
                    if (tmpField[i + 1, j + 1] == Cell.Bomb)
                    {
                        nearbyBombs++;
                    }
                    PlaceMinesCount(field, i - 1, j - 1, nearbyBombs);
                }
            }
        }

        static void FillOpenField(Cell[,] openField)
        {          
            for (int i = 0; i < openField.GetLength(0); i++)
            {
                for (int j = 0; j < openField.GetLength(1); j++)
                {
                    if (i == 0 || j == 0 || i == openField.GetLength(0) - 1 || j == openField.GetLength(1) - 1)
                    {
                        openField[i, j] = Cell.OutSpace;
                    }
                }
            }
        }

        static void PrintField(Cell[,] field)
        {
            for (int i = 0; i < field.GetLength(0); i++)
            {
                for (int j = 0; j < field.GetLength(1); j++)
                {
                    switch (field[i,j])
                    {
                        case Cell.Empty:
                            Console.Write("0 ");
                            break;
                        case Cell.OneMine:
                            Console.Write("1 ");
                            break;
                        case Cell.TwoMines:
                            Console.Write("2 ");
                            break;
                        case Cell.ThreeMines:
                            Console.Write("3 ");
                            break;
                        case Cell.FourMines:
                            Console.Write("4 ");
                            break;
                        case Cell.FiveMines:
                            Console.Write("5 ");
                            break;
                        case Cell.SixMines:
                            Console.Write("6 ");
                            break;
                        case Cell.SevenMines:
                            Console.Write("7 ");
                            break;
                        case Cell.EightMines:
                            Console.Write("8 ");
                            break;
                        case Cell.Flag:
                            Console.Write("B ");
                            break;
                        case Cell.Bomb:
                            Console.Write("X ");
                            break;
                        case Cell.CloseCell:
                            Console.Write(". ");
                            break;
                        case Cell.OutSpace:
                            Console.Write(" ");
                            break;
                    }
                }
                Console.WriteLine();
            }
        }

        static void PrintOurField(Cell[,] field)
        {
            PrintField(field);
            Console.WriteLine();
        }

        static void OpenCells(Cell[,] openField, Cell[,] hiddenField, int iCell, int jCell)
        {
            if (openField[iCell + 1, jCell + 1] == Cell.CloseCell)
            {
                openField[iCell + 1, jCell + 1] = hiddenField[iCell, jCell];

                if (hiddenField[iCell, jCell] == Cell.Empty)
                {
                    openField[iCell + 1, jCell + 1] = hiddenField[iCell, jCell];

                    if(iCell == 0 && jCell == 0)
                    {
                        OpenCells(openField, hiddenField, iCell + 1, jCell);
                        OpenCells(openField, hiddenField, iCell + 1, jCell + 1);
                        OpenCells(openField, hiddenField, iCell, jCell + 1);
                    }
                    else if(iCell == hiddenField.GetLength(0) - 1 && jCell == hiddenField.GetLength(1) - 1)
                    {
                        OpenCells(openField, hiddenField, iCell - 1, jCell);
                        OpenCells(openField, hiddenField, iCell - 1, jCell + 1);
                        OpenCells(openField, hiddenField, iCell, jCell - 1);
                    }
                    else if (iCell == 0 && jCell == hiddenField.GetLength(1) - 1)
                    {
                        OpenCells(openField, hiddenField, iCell + 1, jCell);
                        OpenCells(openField, hiddenField, iCell + 1, jCell - 1);
                        OpenCells(openField, hiddenField, iCell, jCell - 1);
                    }
                    else if (iCell == hiddenField.GetLength(0) - 1 && jCell == 0)
                    {
                        OpenCells(openField, hiddenField, iCell - 1, jCell);
                        OpenCells(openField, hiddenField, iCell - 1, jCell + 1);
                        OpenCells(openField, hiddenField, iCell, jCell + 1);
                    }
                    else if(iCell == 0)
                    {
                        OpenCells(openField, hiddenField, iCell, jCell - 1);
                        OpenCells(openField, hiddenField, iCell, jCell + 1);
                        OpenCells(openField, hiddenField, iCell + 1, jCell - 1);
                        OpenCells(openField, hiddenField, iCell + 1, jCell);
                        OpenCells(openField, hiddenField, iCell + 1, jCell + 1);
                    }
                    else if (jCell == 0)
                    {
                        OpenCells(openField, hiddenField, iCell - 1, jCell);
                        OpenCells(openField, hiddenField, iCell + 1, jCell);
                        OpenCells(openField, hiddenField, iCell - 1, jCell + 1);
                        OpenCells(openField, hiddenField, iCell, jCell + 1);
                        OpenCells(openField, hiddenField, iCell + 1, jCell + 1);
                    }
                    else if (iCell == hiddenField.GetLength(0) - 1)
                    {
                        OpenCells(openField, hiddenField, iCell, jCell - 1);
                        OpenCells(openField, hiddenField, iCell, jCell + 1);
                        OpenCells(openField, hiddenField, iCell - 1, jCell - 1);
                        OpenCells(openField, hiddenField, iCell - 1, jCell);
                        OpenCells(openField, hiddenField, iCell - 1, jCell + 1);
                    }
                    else if (jCell == hiddenField.GetLength(1) - 1)
                    {
                        OpenCells(openField, hiddenField, iCell - 1, jCell);
                        OpenCells(openField, hiddenField, iCell + 1, jCell);
                        OpenCells(openField, hiddenField, iCell - 1, jCell - 1);
                        OpenCells(openField, hiddenField, iCell, jCell - 1);
                        OpenCells(openField, hiddenField, iCell + 1, jCell - 1);
                    }
                    else
                    {
                        OpenCells(openField, hiddenField, iCell - 1, jCell - 1);
                        OpenCells(openField, hiddenField, iCell - 1, jCell);
                        OpenCells(openField, hiddenField, iCell - 1, jCell + 1);
                        OpenCells(openField, hiddenField, iCell, jCell - 1);
                        OpenCells(openField, hiddenField, iCell, jCell + 1);
                        OpenCells(openField, hiddenField, iCell + 1, jCell - 1);
                        OpenCells(openField, hiddenField, iCell + 1, jCell);
                        OpenCells(openField, hiddenField, iCell + 1, jCell + 1);
                    }
                }
            }          
        }

        static void PlayerMove(Cell[,] openField, Cell[,] hiddenField, out bool playGame)
        {
            playGame = true;
            int iCell;
            int jCell;

            Console.WriteLine("Откройте клетку:");

            do
            {
                Console.WriteLine("Введите i:");
                int.TryParse(Console.ReadLine(), out iCell);                               

                Console.WriteLine("Введите j:");
                int.TryParse(Console.ReadLine(), out jCell);

            } while (iCell < 0 || iCell > openField.GetLength(0) - 2 || jCell < 0 || jCell > (openField.GetLength(1) - 2) ||
            (openField[iCell + 1, jCell + 1] != Cell.CloseCell &&
            openField[iCell + 1, jCell + 1] != Cell.Flag));


            if (openField[iCell + 1, jCell + 1] == Cell.CloseCell)
            {
                int moveOrFlag;
                Console.WriteLine("Открыть ячейку или поставить флаг? (0 - открыть, 1 - флаг):");

                OpenCells(openField, hiddenField, iCell, jCell);
                do
                {
                    int.TryParse(Console.ReadLine(), out moveOrFlag);
                } while (moveOrFlag != 0 && moveOrFlag != 1);

                if (moveOrFlag == 0)
                {
                    if (hiddenField[iCell, jCell] == Cell.Bomb)
                    {
                        playGame = false;                        
                    }
                    else
                    {
                        OpenCells(openField, hiddenField, iCell, jCell);
                    }
                }
                else if(moveOrFlag == 1)
                {
                    openField[iCell + 1, jCell + 1] = Cell.Flag;
                }
            }
            else if(openField[iCell + 1, jCell + 1] == Cell.Flag)
            {
                int yesOrNo;
                Console.WriteLine("Убрать флаг? (0 - нет, 1 - да):");
                do
                {
                    int.TryParse(Console.ReadLine(), out yesOrNo);
                } while (yesOrNo != 0 && yesOrNo != 1);

                if (yesOrNo == 0)
                {
                    openField[iCell + 1, jCell + 1] = Cell.Flag;
                }
                else if(yesOrNo == 1)
                {
                    openField[iCell + 1, jCell + 1] = Cell.CloseCell;
                }
            }
        }

        static void VictoryCheck(Cell[,] openField, int bombsCount, out bool victory)
        {
            int stepsCount = (openField.GetLength(0) - 2) * (openField.GetLength(1) - 2);
            victory = false;

            for (int i = 1; i < openField.GetLength(0) - 1; i++)
            {
                for (int j = 1; j < openField.GetLength(1) - 1; j++)
                {
                    if(openField[i, j] != Cell.CloseCell)
                    {
                        stepsCount--;
                    }
                }
            }

            if(stepsCount == bombsCount)
            {
                victory = true;
            }
        }

        static void Main(string[] args)
        {
            int fieldSizeI, fieldSizeJ, bombsCount;
            Level userLevel;
            Cell[,] hiddenField;
            Cell[,] openField;

            int stepsCount;

            bool playGame = true;
            bool victory = false;

            userLevel = ChooseLevel();

            LevelParams(userLevel, out fieldSizeI, out fieldSizeJ, out bombsCount);
            hiddenField = new Cell[fieldSizeI, fieldSizeJ];
            openField = new Cell[fieldSizeI + 2, fieldSizeJ + 2];

            stepsCount = fieldSizeI * fieldSizeJ;

            ClearField(openField); 
            ClearField(hiddenField);

            RandomPlaceMines(hiddenField, bombsCount); 
            FillHiddenField(hiddenField, bombsCount);
            FillOpenField(openField);
            
            while (playGame)
            {
                Console.Clear();
                Console.WriteLine("Игра сапёр: ");
                PrintOurField(openField);
                //PrintOurField(hiddenField);

                PlayerMove(openField, hiddenField,  out playGame);
                VictoryCheck(openField, bombsCount, out victory);
                if (victory == true)
                {
                    playGame = false;
                }
            }                    

            if (playGame == false && victory == true)
            {
                Console.Clear();
                Console.WriteLine("Victory!");
                Console.WriteLine();
                PrintOurField(hiddenField);
            }

            else if(playGame == false && victory == false)
            {
                Console.Clear();
                Console.WriteLine("Wasted");
                Console.WriteLine();
                PrintOurField(hiddenField);
            }
            Console.ReadKey();
        }
    }
}
